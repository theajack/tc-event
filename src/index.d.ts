import {
    IRegistObject,
    IEventListener, IEventRegistOption,
    IEventItem, IEventStatic
} from './type';


export {
    IRegistObject, IEventListener,
    IEventRegistOption, IEventItem
};

declare const event: IEventStatic;

export default event;
